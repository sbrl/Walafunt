<?php

if(!defined("WALAFUNT_ENTRY_POINT"))
	exit("This script isn't meant to be run directly.");

/*
 * @summary Performs the initial setup of the data directory and database.
 * 
 * @returns The time taken to perform the setup.
 */
function setup()
{
	global $settings;
	
	$exec_start = microtime(true);
	
	mkdir($settings->data_dir, 0660);
	touch("$settings->data_dir/sqlite.log");
	
	$db = new PDO("sqlite:$settings->data_dir/$settings->database_filename");
	
	sqlite_gateway::exec("create table users (
	id integer primary key,
	username text,
	password_hash text,
	roles int
);", $db);
	sqlite_gateway::exec("insert into users (id, username, password_hash, roles) values (
	1,
	'admin',
	'" . SQLite3::escapeString(password_hash("password", PASSWORD_DEFAULT, [ "cost" => $settings->password_hash_cost ])) . "',
	" . (ROLE_USER | ROLE_MODERATOR | ROLE_ADMIN) . "
);", $db);
	sqlite_gateway::exec("create table sessions (
	id integer primary key,
	key text,
	username text,
	expiry_timestamp int
);", $db);
	sqlite_gateway::exec("create table code_snippets (
	id integer primary key,
	timestamp int,
	author text,
	title text,
	description text,
	code text,
	language text,
	tags text
);", $db);
	// insert test data here
	sqlite_gateway::exec("insert into code_snippets values (?, 1440668479, 'admin', 'test snippet', 'this is a test snippet.', 'function test(x) { console.log(x); }', 'javascript', 'test, function');", $db);
	
	// todo display a nice welcome page
	
	return microtime(true) - $exec_start;
}

?>
