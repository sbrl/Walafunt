<?php

/*
 * @summary A classs full of user related functions.
 */
class usertils
{
	/*
	 * @summary Checks to see whether a given role code has a particular role assigned. Currently only supports checking for one role at a time.
	 * 
	 * @param $subject The role code to check.
	 * @param $role The role to check $subject for.
	 * 
	 * @returns {boolean} Whether $subject has the role defined in $role.
	 */
	public static function has_role($subject, $role)
	{
		$bin = str_pad(decbin($subject), 3, "0", STR_PAD_LEFT);
		switch($role)
		{
			case ROLE_ANONYMOUS:
				return true;
				
			case ROLE_USER:
				if(substr($bin, 2, 1) == "1") return true;
				return false;
			
			case ROLE_MODERATOR:
				if(substr($bin, 1, 1) == "1") return true;
				return false;
				
			case ROLE_ADMIN:
				if(substr($bin, 0, 1) == "1") return true;
				return false;
				
			default:
				throw new Exception("Unknown role with value '$role'.");
				break;
		}
	}
	
	/*
	 * @summary Hashes $pass with the current default password hash algorithm, according to the settings specified in settings.php.
	 * 
	 * @param {string} $pass The password to hash.
	 * 
	 * @returns {string} The hashed password.
	 */
	public static function hash_password($pass)
	{
		global $settings;
		
		return password_hash($pass, PASSWORD_DEFAULT, [
			"cost" => $settings->password_hash_cost
		]);
	}
	
	/*
	 * @summary Verifies a given username and password against the sqlite database.
	 * 
	 * @param $user {string} The username to check.
	 * @param $pass {string} The password to check.
	 * 
	 * @returns {boolean} Whether the username / password combination is valid.
	 */
	public static function check_credentials($user, $pass)
	{
		global $settings;
		
		$user_data = self::get_user($user);
		// If the user wasn't found, then they can't be valid
		if($user_data === false)
			return false;
		
		// todo check if password needs rehashing
		
		if(password_verify($pass, $user_data["password_hash"]))
		{
			// Calculate the expiry time of the cookies we are about to set
			$expiry_time = time() + $settings->login_expiry_time;
			setcookie("$settings->cookie_prefix-user", $user, $expiry_time);
			// Get a new session token for the current user
			$key = sessions::create($user, $expiry_time);
			setcookie("$settings->cookie_prefix-session-key", $key, $expiry_time);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/*
	 * @summary Gets information about a user.
	 * 
	 * @param $user {string} The username to fetch the details of.
	 * 
	 * @returns The user's details.
	 */
	public static function get_user($user)
	{
		$safe_user = sqlite_gateway::makesafe($user);
		
		$result = sqlite_gateway::query("select * from users where username='$safe_user';");
		if($result === false)
			return false;
		
		return $result->fetch(PDO::FETCH_ASSOC);
	}
}

?>
