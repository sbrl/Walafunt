<?php

class sqlite_gateway
{
	public static function makesafe($str)
	{
		return SQLite3::escapeString($str);
	}
	
	public static function check_query($str)
	{
		if(substr($str, -1) !== ";")
			throw new exception("Missing semicolon from end of sqlite statement.");
	}
	
	public static function exec($query, $handle = null)
	{
		global $db, $settings;
		
		if(empty($handle))
			$handle = $db;
		
		self::check_query($query);
		
		utils::log2file("$settings->data_dir/sqlite.log", "[ " . date("r") . " ] [ exec ] $query");
		
		$handle->exec($query);
	}
	
	public static function query($query, $handle = null)
	{
		global $db, $settings;
		
		if(empty($handle))
			$handle = $db;
		
		self::check_query($query);
		
		utils::log2file("$settings->data_dir/sqlite.log", "[ " . date("r") . " ] [ query ] $query");
		
		return $handle->query($query);
	}
}

?>
