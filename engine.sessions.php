<?php

class sessions
{
	// @summary lookup whether a given key exists in the table.
	//
	// @param $user - The name of the user that should be attached to the key
	// @param $key - The key to lookup.
	// 
	// @returns {boolean} Whether the given key exists for the given user.
	public static function lookup($user, $key)
	{
		// Make the input safe for an sqlite query
		$user = sqlite_gateway::makesafe($user);
		$key = sqlite_gateway::makesafe($key);
		
		$query = sqlite_gateway::query("select * from sessions where key = '$key' and username = '$user';");
		if($query === false) // If the query returns false, it means that it didn't find anything.
			return false; // Tell the caller that we didn't find the key.
		
		$result = $query->fetch(PDO::FETCH_ASSOC);
		
		if($result["username"] == $user &&			// Make sure that the username is the same
		  $result["key"] == $key &&					// Make sure that the key is the same
		  intval($result["expiry_timestamp"]) > time())	// Make sure that the key hasn't expired
		{
			return true;
		}
		else
		{
			return false;
		}
		
		return false;
	}
	
	// @summary creates a new session key in the table.
	//
	// @param $user - The user to attach to he new session key.
	// @param $expiry_time - The time at which the ne wsession key should expire.
	// 
	// @returns {string} - The new key.
	public static function create($user, $expiry_time)
	{
		// Make the input safe for an sqlite query
		$user = sqlite_gateway::makesafe($user);
		$expiry_time = sqlite_gateway::makesafe($expiry_time);
		
		$key = self::generate_key();
		
		$result = sqlite_gateway::exec("insert into sessions values (?, '$key', '$user', $expiry_time);");
		
		return $key;
	}
	
	// @summary Generates a new random secure key.
	public static function generate_key()
	{
		return bin2hex(openssl_random_pseudo_bytes(16));
	}
	
	// @summary Delete a session key from the table.
	// 
	// @param $key - The key to delete.
	public static function delete($key)
	{
		$key = sqlite_gateway::makesafe($key);
		
		$result = sqlite_gateway::exec("delete from sessions where key == '$key';");
		
		if($result > 0)
			return true;
		else
			return false;
	}
	
	// @summary Deletes all old expired session keys
	//
	// @returns {integer} The number keys deleted.
	public static function clean()
	{
		return sqlite_gateway::exec("delete from sessions where expiry_timestamp < " . time() . ";");
	}
}

?>