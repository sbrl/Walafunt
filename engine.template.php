<?php

if(!defined("WALAFUNT_ENTRY_POINT"))
	exit("This script isn't meant to be run directly.");

/*
 * @summary Class that contains a number of functions specifically for working with templates.
 */
class templates
{
	/*
	 * @summary Render a template given it's filename and the components to insert as HTML.
	 * 
	 * @param $template_filename {string} The filename of the template to use. Gets prefixed with template/ so you don't have to.
	 * @param $components The components in key / value format to insert into the template.
	 * 
	 * @returns The rendered HTML.
	 */
	public static function render_html($template_filename, $components)
	{
		$page = file_get_contents("templates/$template_filename");
		
		// Render the header first - it ~~might~~ will contain other things that will need replacing in step 2
		$page = str_replace([
			"{header}"
		], [
			file_get_contents("templates/header.html")
		], $page);
		
		$page = str_replace(array_keys($components), array_values($components), $page);
		
		$page .= "\n";
		
		return $page;
	}
	
	/*
	 * @summary Renders an array of snippets to HTML.
	 * 
	 * @param $snippets The array of snippets to render.
	 * @param $preview = false {boolean} Whether to render the snippets in preview mode. Currently doesn't do anything.
	 * 
	 * @returns The rendered HTML.
	 */
	public static function render_snippets($snippets, $preview = false)
	{
		// todo Use the $preview parameter
		// todo Accept a single snippet as wel as an array of snippets
		// note We might want to split the rendering of a snippet and an array of snippets into 2 separate functions
		$fragment = "";
		foreach ($snippets as $snippet)
		{
			$fragment .= self::render_html("snippet.html", [
				"{code}" => $snippet["code"],
				"{title}" => $snippet["title"],
				"{author}" => $snippet["author"],
				"{language}" => $snippet["language"],
				"{description}" => $snippet["description"],
				"{tags}" => self::format_tags(array_map("trim", explode(",", $snippet["tags"]))),
			]);
		}
		
		return $fragment;
	}
	
	/*
	 * @summary Format's an array of tags ready for display.
	 * 
	 * @param $tags {array} The array of tags to format
	 * 
	 * @returns {string} The formatted tags.
	 */
	public static function format_tags($tags)
	{
		return "#" . implode(", #", $tags);
	}
	
	/*
	 * @summary Prepends a character to a username based on the given role.
	 * 
	 * @param $username {string} The username to render.
	 * @param $role {number,role} The role to use to determine the character to prepend.
	 * 
	 * @returns {string} The username with a character prepended to it.
	 */
	public static function render_user($username, $role)
	{
		if(usertils::has_role($role, ROLE_ADMIN)) {
			return "&#128024;$username";
		} else if(usertils::has_role($role, ROLE_MODERATOR)) {
			return "&#127775;$username";
		} else if(usertils::has_role($role, ROLE_USER)) {
			return $username;
		} else if(usertils::has_role($role, ROLE_ANONYMOUS)) {
			return $username;
		}
	}
	
	/*
	 * @summary Renders a HTML user panel for the given user and role.
	 * 
	 * @param $username {string} The username to use for rendering.
	 * @param $role {number,role} The role to use for rendering.
	 * 
	 * @returns {string} An HTML user panel.
	 */
	public static function render_user_panel($username, $role)
	{
		$result = "";
		if(!usertils::has_role($role, ROLE_USER))
		{
			$result .= "Browsing as " . self::render_user($username, $role) . ". <a href='?action=login'>Login</a>.";
		}
		else
		{
			$result .= "Logged in as " . self::render_user($username, $role) . ". <a href='?action=logout'>Logout</a>.";
		}
		
		return $result;
	}
	
	/*
	 * @summary Renders a HTML drop down list of languages.
	 * 
	 * @returns An HTML drop down list of languages.
	 */
	public static function render_lang_list()
	{
		$langs = file("langs.txt");
		
		$result = "<select name='language'>\n";
		foreach($langs as $lang)
		{
			$result .= "<option value='" . strtolower(trim($lang)) . "'>" . $lang . "</option>\n";
		}
		$result .= "</select>\n";
	}
}

?>
