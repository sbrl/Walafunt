<?php

if(isset($_COOKIE["$settings->cookie_prefix-user"]) &&
  isset($_COOKIE["$settings->cookie_prefix-session-key"]))
{
	// The requester has attached a session key, we should probably take a look at it.
	if(sessions::lookup($_COOKIE["$settings->cookie_prefix-user"],
					$_COOKIE["$settings->cookie_prefix-session-key"]))
	{
		// The user's key was valid! Update the environment to reflect the user.
		env::$username = $_COOKIE["$settings->cookie_prefix-user"];
		env::$key = $_COOKIE["$settings->cookie_prefix-session-key"];
		env::$role = intval(usertils::get_user(env::$username)["roles"]);
	}
}

?>
