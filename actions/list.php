<?php

$exec_start = microtime(true);

if(!defined("WALAFUNT_ENTRY_POINT"))
	exit("This script isn't meant to be run directly.");

$params = utils::apply_default_params($_GET, [
	"language" => "all",
	"tags" => "all",
	"sort" => "timestamp",
	"sort_dir" => "asc",
	"page" => "1",
]);

$tags = array_map("trim", explode(",", $params["tags"]));

if($params["language"] == "all") $params["language"] = "*";
if($params["tags"] == "all") $params["tags"] = "*";

$offset = ($params["page"] - 1) * $settings->snippets_per_page;

$snippets = sqlite_gateway::query("select * from code_snippets; limit $settings->snippets_per_page offset $offset;");
if($snippets === false) // Make sure that the return of the query is always iterable
	$snippets = [];

$tag_str = templates::format_tags($tags);

$components = [
	"{prefix}" => ucwords(($params["tags"] == "*" ? "" : $tags_str . " - ") . ($params["language"] == "*" ? "all snippets" : $params["language"] . " - ")),
	"{code-snippets-list}" => templates::render_snippets($snippets),
	"{user-panel}" => templates::render_user_panel(env::$username, env::$role)
	// todo calculate and render pagination
];


header("x-time-taken: " . (microtime(true) - $exec_start));
echo(templates::render_html("main.html", $components));

?>
