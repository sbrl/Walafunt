<?php

if($_SERVER["REQUEST_METHOD"] == "GET")
{
	exit(file_get_contents("templates/login.html"));
}

// The request isn't GET, so it's probably POST.
utils::require_params($_POST, [
	"username",
	"password"
], "POST");

if(usertils::check_credentials($_POST["username"], $_POST["password"]))
{
	env::$username = $_POST["username"];
	env::$role = usertils::get_user($_POST["username"])["roles"];
	
	http_response_code(302); // temporary redirect
	header("location: index.php?action=list&login=success");
	
	// todo set an *appropriate* cookie here
	
	// todo output a nice login success page here?
	exit();
}

// The login failed!
http_response_code(401);
// todo output a nice login failed page here.
header("content-type: text/plain");
exit("Login failed.");

?>
