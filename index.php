<?php

/*
 * actions:
	* list (default)	* Lists code snippets.
		* language		* The language(s) to show. Default: all.
		* tags			* The tag(s) to show. Default: all.
		* sort			* What to sort the snippets by. Default: timestamp.
		* sort_dir		* Which direction to sort the snippets in. Default: asc.
	* settings		* Shows a settings page or sets a setting for a user.
		* key		* The key of the settings to set. Default: none.
		* value		* The value to set the setting to. Default: none.
	* login				* Shows a login page or logs a user in.
		* [POST] user	* The username to login.
		* [POST] pass	* The password of given user.
	* logout		* Logs the currrent user out.
	* view		* Shows the code snippet associated with a given id.
		* id	* The id of the code snippet to show. Default: none.
		* raw	* Whether the output should be raw (i.e. Not wrapped in HTML). Default: false.
	* edit			* Edits a code snippet or creates a new one
		* (see schema for the code_snippets table)
	* delete	* Deletes the code snippet with the given id.
		* id	* The id of the snippet to delete.
 */

// Define the magic constant that tells the other files that it's ok to run
define("WALAFUNT_ENTRY_POINT", true);

// Set up a few things ready for later on
require("settings.php");
require("engine.php");

// Call over a few friends to join the party
require("engine.utils.php");
require("engine.usertils.php");
require("engine.sessions.php");
require("engine.template.php");
require("gateway.sqlite.php");

// Check to see if the data directory exists. If it doesn't, then we need to run the setup.
if(!file_exists("$settings->data_dir/"))
{
	require("setup.php");
	
	exit("Initial setup completed in " . setup() . "ms. Reload the page to start using walafunt.");
}

// Open the database
$db = new PDO("sqlite:$settings->data_dir/$settings->database_filename");

// Start the music
require("env.php");

$action = "";
if(!isset($_GET["action"]))
	$action = $settings->default_action;
else
	$action = $_GET["action"];

$action = preg_replace("/[^a-z0-9\-]/i", "", $action);

$action_path = "actions/$action.php";

if(!file_exists($action_path))
{
	// todo send fancy error message
	header("content-type: text/plain");
	exit("Unknown action $action.");
}

require($action_path);

?>
