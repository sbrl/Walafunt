<?php

if(!defined("WALAFUNT_ENTRY_POINT"))
	exit("This script isn't meant to be run directly.");

// User roles
define("ROLE_ANONYMOUS", 0);
define("ROLE_USER", 1);
define("ROLE_MODERATOR", 2);
define("ROLE_ADMIN", 4);

/*
 * @summary Contains a number of environment variables that give information about the current request.
 */
class env
{
	public static $username = "anonymous";
	public static $key = "";
	public static $role = ROLE_ANONYMOUS;
}

?>
