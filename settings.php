<?php

if(!defined("WALAFUNT_ENTRY_POINT"))
	exit("This script isn't meant to be run directly.");

$settings = new stdClass();

// Path to the directory in which to store all of our data. *Don't* include the trailing slak.
$settings->data_dir = "data";
// The filename to use for the database.
$settings->database_filename = "code_snippets.sqlite";

// The cost to use when hashing passwords.
$settings->password_hash_cost = 12;

// The default action. Usually you don't need to change this.
$settings->default_action = "list";

// The number of snippets to display per page.
$settings->snippets_per_page = 25;

// How long to remember that a user it logged in for.
$settings->login_expiry_time = 60 * 60 * 24 * 7; // 7 days

// The prefix to use when setting / getting cookies.
// Note that changing this will log everyone out!
$settings->cookie_prefix = "walafunt";



// Not used yet //
$settings->allow_anonymous_viewing = true;
$settings->allow_anon_registrations = false;
//////////////////

?>
