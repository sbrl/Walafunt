<?php
/*
 * @summary Class that holds various utility functions.
 */
class utils
{
	/*
	 * @summary Log $msg to $filename.
	 * 
	 * @param $msg {string} The string to append to the log file.
	 * @param $filename {string} The path to the file to append the message to.
	 */
	public static function log2file($filename, $msg)
	{
		if(!file_exists($filename))
			throw new Exception("utils::log2file: Can't find file $filename.");
		
		file_put_contents($filename, "$msg\n", FILE_APPEND);
	}
	
	/*
	 * @summary Given an array and an array of expected keys, this function exits with a fatal error if any of the given keys are not found.
	 * 
	 * @param $array The array to check.
	 * @param $params An array of keys to look for.
	 * @param $type The type of paramters we are dealing with. Only used to make the error message more informative.
	 */
	public static function require_params($array, $params, $type)
	{
		foreach($params as $param)
		{
			if(!array_key_exists($param, $array))
			{
				header("content-type: text/plain");
				exit("Fatal Error: missing $type parameter $param.");
			}
		}
	}
	
	/*
	 * @summary Given an array and a second array of key values, this function copies the key / value pairs from the second array to the first - so long as there isn't a key with that name already.
	 * 
	 * @param $array The first array of key / value pairs.
	 * @param $defaults The array of 'default' key / value pairs that should be copied over to the first array if it doesn't have any of the given keys.
	 * 
	 * @returns The first array with all of the defauls copied in.
	 */
	public static function apply_default_params($array, $defaults)
	{
		foreach($defaults as $key => $value)
		{
			if(!array_key_exists($key, $array))
				$array[$key] = $value;
		}
		
		return $array;
	}
}

?>
