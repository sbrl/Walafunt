# Walafunt
A multi user code snippet organisation tool

_This project is a work in progress_

## Planned features
 - Multi user setup
 - Create / edit / delete code snippets
 - Each code snippet will have:
	- A title
	- A description
	- An author (see bullet point 1)
	- A language
	- One or more tags
 - Admin panel to configure settings

Later on, I want to add these features too:
 - Commenting: The ability to comment on a code snippet would probably be really useful. Perhaps a specific line number could be specified too.
 - Token based register system: The admin can create a random token to give to someone in order to let them create a new account on their own

If I have time, I think that these features would be interesting:

 - Starring: The ability to star a given codee snippet might be useful.
 - netcat support: Like [termbin.com](http://termbin.com), I think it would be cool to allow uploads through netcat.

## Quickstart
This project is a work in progress. If you want to check it out anyway, simply clone this repository and serve it from any web server with PHP support. You'll need the SQLite modules (for data storage, both Sqlite3 and PDO), and OpenSSL (for secure session key generation) too.

## License
Currently I'm using the Mozilla Public License, which you can find in the `LICENSE.md` file in this repository.